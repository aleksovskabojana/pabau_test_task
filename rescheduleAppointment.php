<?php
require_once 'classes/appointments.php';

$appointment = new Appointments();
$appointment->checkIfAppointmentExists($_POST);
$appointment->rescheduleAppointment($_POST);
