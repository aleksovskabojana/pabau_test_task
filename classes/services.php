<?php

require_once 'databaseConnection.php';

class Services extends DatabaseConnection{
    
    public function fetchServices()
    {
        $sql = "SELECT * FROM `services`";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
    }

}