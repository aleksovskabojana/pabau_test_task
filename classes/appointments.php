<?php

require_once 'databaseConnection.php';

class Appointments extends DatabaseConnection{
    private $serviceId;
    private $date;
    private $startTime;
    private $endTime;
    private $appointmentNotes;
    private $fullName;
    private $phone;
    private $id;

    // Setters
    public function setServiceId($serviceId) {
        $this->serviceId = $serviceId;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setStartTime($startTime) {
        $this->startTime = $startTime;
    }

    public function setEndTime($endTime) {
        $this->endTime = $endTime;
    }

    public function setAppointmentNotes($appointmentNotes) {
        $this->appointmentNotes = $appointmentNotes;
    }

    public function setFullName($fullName) {
        $this->fullName = $fullName;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    // Getters
    public function getServiceId() {
        return $this->serviceId;
    }

    public function getId() {
        return $this->id;
    }

    public function getDate() {
        return $this->date;
    }

    public function getStartTime() {
        return $this->startTime;
    }

    public function getEndTime() {
        return $this->endTime;
    }

    public function getAppointmentNotes() {
        return $this->appointmentNotes;
    }

    public function getFullName() {
        return $this->fullName;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function calculateEndTime()
    {
        $endTime = date('H:i:s', strtotime($this->getStartTime() . '+' . $this->getEndTime()));
        return $endTime;
    }

    public function checkIfAppointmentExists($data)
    {
        $this->setDate($data['date']);
        $this->setStartTime($data['startTime']);
        $this->setEndTime($data['endTime']);

        $endTime = $this->calculateEndTime();

        $sql = "SELECT * FROM `appointments`
                WHERE `date` = :date
                AND ((:start_time >= `start_time` AND :start_time < `end_time`) OR (:end_time > `start_time` AND :end_time <= `end_time`))
                AND `status` = 'scheduled';";
       
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([
                        'date' => $this->getDate(),
                        'start_time' => $this->getStartTime(),
                        'end_time' => $endTime
                       ]);
        $result = $stmt->fetchAll();
       
        if($result){
            echo json_encode([
                              'status' => 'error', 
                              'message' => 'The selected appointment slot is busy. Please choose another time.'
                            ]);
            die();
        }
    }

    public function bookAnAppointment($data)
    {
        $this->setServiceId($data['service_id']);
        $this->setAppointmentNotes($data['appointmentNotes']);
        $this->setFullName($data['fullName']);
        $this->setPhone($data['phone']);

        $endTime = $this->calculateEndTime();

        $sql = "INSERT INTO `appointments` (`service_id`, `date`, `start_time`, `end_time`, `status`, `notes`, `patient_name`, `phone`)
                VALUES (:service_id, :date, :start_time, :end_time,  :status, :notes, :patient_name, :phone)";

        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute([
                                    'service_id' => $this->getServiceId(),
                                    'date' => $this->getDate(),
                                    'start_time' => $this->getStartTime(),
                                    'end_time' => $endTime,
                                    'status' => 'scheduled',
                                    'notes' => $this->getAppointmentNotes(),
                                    'patient_name' => $this->getFullName(),
                                    'phone' => $this->getPhone()                           
                                ]);

        if($result){
            echo json_encode(['status' => 'success', 'message' => 'Appointment created successfully']);
        }else{
            echo json_encode(['status' => 'error', 'message' => 'Something went wrong']);
        }

    }

    public function fetchAppointments()
    {
        $sql = "SELECT appointments.id AS appointment_id, appointments.*, services.name 
                FROM `appointments`
                JOIN services
                ON services.id = appointments.service_id
                WHERE status = 'scheduled'
                ORDER BY `date`;";
        
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result;
      
    }

    public function renderAppointments($appointments)
    {
        foreach($appointments as $appointment){
            echo "
            <tr id='{$appointment['appointment_id']}'>
                <th scope='row'>{$appointment['appointment_id']}</th>
                <td>{$appointment['patient_name']}</td>
                <td>{$appointment['phone']}</td>
                <td>{$appointment['date']}</td>
                <td>{$appointment['start_time']}</td>
                <td>{$appointment['end_time']}</td>
                <td>{$appointment['name']}</td>
                <td>
                    <button type='submit' class='cancel-btn' data-id='{$appointment['appointment_id']}'>Cancel</button>                 
                    <a href='editAppointment.php?id={$appointment['appointment_id']}'>Reshedule</a>
                </td>
            </tr>
            ";
        }
    }

    public function showAppointment($id)
    {
        $sql = "SELECT appointments.id AS appointment_id, appointments.*, services.name 
                FROM `appointments` 
                JOIN services
                ON services.id = appointments.service_id 
                WHERE appointments.id = :id";
                
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute(['id' => $id]);
        $result = $stmt->fetch();

        return $result;

    }

    public function rescheduleAppointment($data)
    {
        $this->setId($data['id']);

        $endTime = $this->calculateEndTime();
                
        $sql = "UPDATE `appointments` 
                SET `date` = :date, `start_time` = :start_time, `end_time` = :end_time 
                WHERE `appointments`.`id` = :id;";                
        
        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute([
                                  'date' => $this->getDate(),
                                  'start_time' => $this->getStartTime(),
                                  'end_time' => $endTime,
                                  'id' => $this->getId()
                                ]);

        if ($result) {
            echo json_encode(['status' => 'success', 'message' => 'Appointment rescheduled successfully']);
        } else {
            echo json_encode(['status' => 'error', 'message' => 'Something went wrong']);
        }

    }

    public function cancelAppointment($id)
    {
        $sql = "UPDATE `appointments` 
                SET `status` = 'canceled' 
                WHERE `appointments`.`id` = :id;";

        $stmt = $this->pdo->prepare($sql);
        $result = $stmt->execute(['id' => $id]);

        if($result){
            echo json_encode(['status' => 'success']);
        }else {
            echo json_encode(['status' => 'error']);
        }
    }

 
}