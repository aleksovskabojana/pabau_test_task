<?php
require_once 'partials/header.html';
require_once 'classes/appointments.php';
$appointment = new Appointments();
$result = $appointment->showAppointment($_GET['id']);
?>

    <div class="container mt-5">   
      <a href="appointmentList.php" class="mb-3">Go back</a>    
      <div class="alert alert-danger d-none" role="alert" id="status-error"></div>
        <div class="alert alert-success d-none" role="alert" id="status-success"></div> 
        <div>
            <h3>Appointment Details</h3>
            <p>Patient name: <?= $result['patient_name']?></p>
            <p>Contact: <?= $result['phone']?></p>
            <p>Notes: <?= $result['notes']?></p>
            <p>Service: <?= $result['name']?></p>
            <p>Date: <span id="new-date"><?= $result['date']?></span></p>
            <p>Start Time: <span id="new-start-time"><?= $result['start_time']?></span></p>
            <p>End Time: <span id="new-end-time"><?= $result['end_time']?></span></p>

            <div class="form shadow borderx">
                <h5>Reschedule</h5>           
             
                <input type="hidden" value="<?= $result['appointment_id'] ?>" name="id" id="appointment_id">

                <label for="date">Date</label><br>
                <input type="text" id="myDatePicker" name="date"><br><br>

                <label for="startTime">Start Time</label>
                <select name="startTime" id="startTime">
                    <option value="09:00">09:00</option>
                    <option value="09:30">09:30</option>

                    <option value="10:00">10:00</option>
                    <option value="10:30">10:30</option>

                    <option value="11:00">11:00</option>
                    <option value="11:30">11:30</option>
                    
                    <option value="12:00">12:00</option>
                    <option value="12:30">12:30</option>

                    <option value="13:00">13:00</option>
                    <option value="13:30">13:30</option>

                    <option value="14:00">14:00</option>
                    <option value="14:30">14:30</option>
                    
                    <option value="15:00">15:00</option>
                    <option value="15:30">15:30</option>

                    <option value="16:00">16:00</option>
                    <option value="16:30">16:30</option>

                </select>
            
            <label for="endTime">Duration</label>
            <select name="endTime" id="endTime">
                <option value="30 minutes">30 mins</option>
                <option value="1 hour">1 hour</option>
            </select>
                <br>
                <button type="submit" id="submit-btn" class="mt-3">Submit</button>
            </div>

        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script>
        flatpickr("#myDatePicker", {
            disable: [
                function(date) {
                    return date.getDay() === 0 || date.getDay() === 6; 
                }
            ],
            minDate: "today"
        });

        $(document).ready(function() {
            $("#submit-btn").on("click", function(e) {
                e.preventDefault();
              
                let date = $("#myDatePicker").val();
                let startTime = $("#startTime").val();
                let endTime = $("#endTime").val();
                let id = $("#appointment_id").val();
               
                let formData = {
                    date: date,
                    startTime: startTime,
                    endTime: endTime,
                    id: id                    
                };

                $.ajax({
                    type: "POST",
                    url: "rescheduleAppointment.php",
                    data: formData,
                    dataType: "json",
                }).done(function(data){
                    if(data.status == "success"){  
                        //Show alert   
                        $("#status-error").addClass("d-none");
                        $("#status-success").removeClass("d-none");
                        $("#status-success").text(data.message);
                        $("#status-success").fadeOut(2000);

                        // Updating values
                        $("#new-date").text($("#myDatePicker").val());
                        $("#new-start-time").text($("#startTime").val())
                        $("#new-end-time").text($("#endTime").val())
                        
                    }else if(data.status == "error"){
                        $("#status-error").removeClass("d-none");
                        $("#status-error").text(data.message);
                    }
                }).fail(function(error){
                    alert("An error occurred: " + error);
                })
            });
        });
    </script>
  </body>
</html>