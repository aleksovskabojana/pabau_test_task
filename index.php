<?php
require_once 'partials/header.html';
require_once 'classes/services.php';
            
$service = new Services();
$allServices = $service->fetchServices();   
?>
    <div class="container py-5 d-flex justify-content-center align-items-center">
        <div>
        <a href="appointmentList.php" class="mb-3">See all appointments</a><br>

        <div class="alert alert-danger d-none" role="alert" id="status-error"></div>
        <div class="alert alert-success d-none" role="alert" id="status-success"></div>
        <div class="form shadow border">
            <div class="form-header">
                <h5>Book Appointment</h5>            
            </div>
        
            <label for="service">Service</label><br>
            <select name="service_id" id="service">
                <option value="" disabled selected>Select a service</option>

                <?php foreach($allServices as $service){ ?>
                    <option value='<?=$service['id']?>'><?=$service['name']?></option>
                <?php } ?>

            </select><br>
            
            <label for="myDatePicker">Date</label><br>
            <input type="text" id="myDatePicker" name="date"><br><br>
           
            <label for="startTime">Start Time</label>
            <select name="startTime" id="startTime">

                <option value="09:00">09:00</option>
                <option value="09:30">09:30</option>

                <option value="10:00">10:00</option>
                <option value="10:30">10:30</option>

                <option value="11:00">11:00</option>
                <option value="11:30">11:30</option>
                
                <option value="12:00">12:00</option>
                <option value="12:30">12:30</option>

                <option value="13:00">13:00</option>
                <option value="13:30">13:30</option>

                <option value="14:00">14:00</option>
                <option value="14:30">14:30</option>
                
                <option value="15:00">15:00</option>
                <option value="15:30">15:30</option>

                <option value="16:00">16:00</option>
                <option value="16:30">16:30</option>

            </select>
            
            <label for="endTime">Duration</label>
            <select name="endTime" id="endTime">
                <option value="30 minutes">30 mins</option>
                <option value="1 hour">1 hour</option>
            </select>            
            <br>
            <label for="fullName">Full Name</label><br>
            <input type="text" name="fullName" id="fullName">
            <br>
            <label for="phone">Phone</label><br>
            <input type="text" name="phone" id="phone"><br>
            <br>
            <label for="appointmentNotes">Appointment Notes:</label><br>
            <textarea name="appointmentNotes" id="appointmentNotes" cols="30" rows="5" placeholder="Please provide any special requirements or medical conditions."></textarea><br><br>

            <button type="submit" id="submit-btn">Submit</button>
        </div>
</div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script>
        flatpickr("#myDatePicker", {
            disable: [
                function(date) {
                    return date.getDay() === 0 || date.getDay() === 6; 
                }
            ],
            minDate: "today"
        });

        $(document).ready(function() {
            $("#submit-btn").on("click", function(e) {
                e.preventDefault();

                let service_id = $("#service").val();
                let date = $("#myDatePicker").val();
                let startTime = $("#startTime").val();
                let endTime = $("#endTime").val();
                let appointmentNotes = $("#appointmentNotes").val();
                let fullName = $("input[name='fullName']").val();
                let phone = $("input[name='phone']").val();

                let formData = {
                    service_id: service_id,
                    date: date,
                    startTime: startTime,
                    endTime: endTime,
                    appointmentNotes: appointmentNotes,
                    fullName: fullName,
                    phone: phone
                };
                
                $.ajax({
                    type: "POST",
                    url: "bookAppointment.php",
                    data: formData,
                    dataType: "json",
                }).done(function(data){

                    if(data.status == "success"){
                        // Clear input values
                        $("#service").val("");
                        $("#myDatePicker").val("");
                        $("#appointmentNotes").val("");
                        $("input[name='fullName']").val("");
                        $("input[name='phone']").val("");

                        // Show alert
                        $("#status-error").addClass("d-none");
                        $("#status-success").removeClass("d-none");
                        $("#status-success").text(data.message);
                        $("#status-success").fadeOut(2000);

                    }else if(data.status == "error"){
                        $("#status-error").removeClass("d-none");
                        $("#status-error").text(data.message);
                    }
                }).fail(function(error){
                    alert("An error occurred: " + error);
                })
            });
        });

    </script>
  </body>
</html>