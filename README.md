# Appointment Management System

## Description
This is an appointment management system designed to help users schedule and manage appointments for various services.

## Before You Begin
Before you start using the Appointment Management System, there are a few important things to note:

1. **Validation:** Please be aware that this version of the application does not include input validation. Make sure to provide valid values in the input fields to avoid errors.

2. **Appointment Slot Availability:** The application includes a check for appointment slot availability. This means that only one appointment can be scheduled for a specific time slot. 

3. **PHP OOP:** The code is organized using Object-Oriented Programming principles.

4. **CRUD Operations:** The application supports Create, Read, Update, and Delete (Soft Delete) operations for appointments. All these operations are implemented using Ajax and jQuery.

5. **Styling:** The user interface is built using Bootstrap and custom CSS for styling.


## Features

- Schedule appointments for different services.
- Check appointment availability and avoid conflicts.
- View a list of all scheduled appointments.
- Reschedule or cancel appointments as needed.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- PHP 
- MySQL database

## Installation

1. Clone this repository to your local machine: https://gitlab.com/aleksovskabojana/pabau_test_task.git
2. Create a MySQL database and update the database configuration in classes/databaseConnection.php
3. Import the database schema using the provided SQL file: doctor_appointment.sql


