<?php
require_once 'partials/header.html';
?>

    <div class="container mt-5">
      <a href="index.php" class="mb-3">Book Appointment</a>
    <table class="table">
    <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Patient Name</th>
          <th scope="col">Phone</th>
          <th scope="col">Date</th>
          <th scope="col">Start Time</th>
          <th scope="col">End Time</th>
          <th scope="col">Name</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>   
      <?php
            require_once 'classes/appointments.php';
            
            $appointment = new Appointments();
            $appointments = $appointment->fetchAppointments();
            $appointment->renderAppointments($appointments);

      ?>
      </tbody>
    </table>

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>

    <script>
      $(".cancel-btn").on("click", function(e){
        e.preventDefault();

        let appointment_id = $(this).data("id");
        let self = this;

        $.ajax({
          type: "POST",
          url: "cancelAppointment.php",
          data: {appointment_id : appointment_id},
          dataType: "json"
        }).done(function(data){
          if(data.status === 'success'){
            $("#" + $(self).data("id")).remove();
          }
        }).fail(function(error){
          alert("An error occurred: " + error);
        })
      })
    </script>
  </body>
</html>